var webpack = require('webpack')
var path = require('path')
var PrerenderSpaPlugin = require('prerender-spa-plugin')
var Renderer = PrerenderSpaPlugin.PuppeteerRenderer

module.exports = {
	css: {
		loaderOptions: {
			sass: {
				data: "@import '~@/stylesheets/core.scss';",
				includePaths: [
					require("bourbon-neat").includePaths,
				]
			}
		}
	},
	configureWebpack: {
		plugins: process.env.NODE_ENV == 'production' ? [
			new webpack.DefinePlugin({
				'process.env': {
				// 'API_URL': JSON.stringify(process.env.API_URL)
				'MAPS_KEY': JSON.stringify(process.env.MAPS_KEY),
				'API': JSON.stringify(process.env.API)
				},
			}),
			new PrerenderSpaPlugin({
				staticDir: path.join(__dirname, './dist'),
				routes: [
					'/',
					'/landing',
				],
				renderer: new Renderer({
					injectProperty: '__PRERENDER_INJECTED',
					inject: {
						prerendered: true
					},
					renderAfterDocumentEvent: 'app.rendered'
				})
			})
		] : [
			new webpack.DefinePlugin({
				'process.env': {
				// 'API_URL': JSON.stringify(process.env.API_URL)
				'MAPS_KEY': JSON.stringify(process.env.MAPS_KEY),
				'API': JSON.stringify(process.env.API)
				}
			})
		]
	},
	devServer: {
        proxy: process.env.API
    }
}
