import { storiesOf } from '@storybook/vue'
import AMSpecs from './index'

storiesOf('Molecule - am-specs', module)
	.add('default', () => ({
		components: {
			'am-specs': AMSpecs
		},
		template: `
			<am-specs />
		`
	}))
