import { storiesOf } from '@storybook/vue'
import AMMapImage from './index'

storiesOf('Molecule - am-map-image', module)
	.add('default', () => ({
		components: {
			'am-map-image': AMMapImage
		},
		template: `
			<am-map-image
				image="/images/blueprint.png"
			/>
		`
	}))
