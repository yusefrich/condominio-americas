import { storiesOf } from '@storybook/vue'
import AMInputText from './index'

storiesOf('Molecule - am-input-text', module)
	.add('default', () => ({
		components: {
			'am-input-text': AMInputText
		},
		data: () => ({
			text: ''
		}),
		template: `
			<am-input-text v-model="text" />
		`
	}))
