import { storiesOf } from '@storybook/vue'
import AMControl from './index'

storiesOf('Molecule - am-control', module)
	.add('default', () => ({
		data: () => ({
			container: {
				display: 'flex',
				flexFlow: 'row wrap',
				alignItems: 'center',
				justifyContent: 'center'
			}
		}),
		components: { 'am-control': AMControl },
		template: `
		<div :style="container">
			<am-control />
		</div>
		`
	}))

