import { storiesOf } from '@storybook/vue'
import AMInputTextarea from './index'

storiesOf('Molecule - am-input-textarea', module)
	.add('default', () => ({
		components: {
			'am-input-textarea': AMInputTextarea
		},
		data: () => ({
			text: ''
		}),
		template: `
			<am-input-textarea v-model="text" />
		`
	}))
