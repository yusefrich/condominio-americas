import { storiesOf } from '@storybook/vue'
import AMGallery from './index'

storiesOf('Molecule - am-gallery', module)
	.add('default', () => ({
		components: { 'am-gallery': AMGallery },
		template: `
			<am-gallery />
		`
	}))
