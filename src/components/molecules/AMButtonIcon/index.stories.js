import { storiesOf } from '@storybook/vue'
import AMButtonIcon from './index'

storiesOf('Molecule - am-button-icon', module)
	.add('default', () => ({
		data: () => ({
			container: {
				display: 'flex',
				flexFlow: 'row wrap',
				alignItems: 'center',
				justifyContent: 'space-around'
			}
		}),
		components: { 'am-button-icon': AMButtonIcon },
		template: `
		<div :style="container">
			<am-button-icon
				theme="primary"
				name="close"
				size="sm"
			/>
			<am-button-icon
				theme="primary"
				name="play"
				size="md"
			/>
		</div>
		`
	}))

