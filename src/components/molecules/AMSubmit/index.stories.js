import { storiesOf } from '@storybook/vue'
import AMSubmit from './index'

storiesOf('Molecule - am-submit', module)
	.add('default', () => ({
		components: {
			'am-submit': AMSubmit
		},
		data: () => ({
			state: 'idle'
		}),
		template: `
			<am-submit :state="state" @click="onClick" />
		`,
		methods: {
			onClick(current) {
				if (this.state == 'idle')
					this.state = 'loading'
				else if (this.state == 'loading')
					this.state = 'done'
				else if (this.state == 'done')
					this.state = 'idle'
			}
		}
	}))
