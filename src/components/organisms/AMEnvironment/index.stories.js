import { storiesOf } from '@storybook/vue'
import AMEnvironment from './index'
import store from '@/store'

storiesOf('Organism - am-environment', module)
	.add('default', () => ({
		store: store,
		components: {
			'am-environment': AMEnvironment
		},
		data: () => ({
			//
		}),
		template: `
			<am-environment />
		`
	}))
