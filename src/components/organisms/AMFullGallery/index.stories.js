import { storiesOf } from '@storybook/vue'
import AMFullGallery from './index'

storiesOf('Organism - am-full-gallery', module)
	.add('default', () => ({
		components: {
			'am-full-gallery': AMFullGallery
		},
		data: () => ({
			//
		}),
		template: `
			<am-full-gallery :style="{ height: '640px' }" />
		`
	}))
