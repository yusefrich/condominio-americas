import { storiesOf } from '@storybook/vue'
import AMAbout from './index'

storiesOf('Organism - am-about', module)
	.add('default', () => ({
		components: { 'am-about': AMAbout },
		template: `
			<am-about />
		`
	}))
