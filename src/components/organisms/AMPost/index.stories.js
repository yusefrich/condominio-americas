import { storiesOf } from '@storybook/vue'
import AMPost from './index'

storiesOf('Organism - am-post', module)
	.add('default', () => ({
		components: {
			'am-post': AMPost
		},
		data: () => ({
			//
		}),
		template: `
			<am-post />
		`
	}))
