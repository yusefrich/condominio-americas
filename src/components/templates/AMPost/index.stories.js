import { storiesOf } from '@storybook/vue'
import AMTimelineInside from './index'

storiesOf('Template - am-post', module)
	.add('default', () => ({
		components: {
			'am-post': AMTimelineInside
		},
		data: () => ({
			view: {
				height: '800px'
			}
		}),
		template: `
			<am-post :style="view" />
		`
	}))
