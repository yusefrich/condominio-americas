import Vue from 'vue'
import VueFullPage from 'vue-fullpage.js'
import App from './App.vue'
import router from './router'
import store from './store'
import BootstrapVue from 'bootstrap-vue/dist/bootstrap-vue.esm';

import moment from 'moment'
// bootstrap styles
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

// normal styles
import './stylesheets/defaults.scss'

Vue.config.productionTip = false
Vue.use(VueFullPage)
Vue.use(BootstrapVue);

Vue.filter('formatDate', function(value) {
  if (value) {
    moment.locale('pt-br');
    //novo pipe de formatação
    return moment(String(value)).format('MMM [de] YYYY')
  }
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
