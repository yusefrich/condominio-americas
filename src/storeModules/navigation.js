import Vue from 'vue'
import store from '@/store'

export default {
	namespaced: true,

	state: {
		active: false,
		current: 'about',
		items: {
			'about': 'Sobre',
			'album': 'Galeria',
			'environment': 'Ambientes e Lazer',
			'chill': 'Alto Padrão',
			'safety': 'Segurança',
			'lifestyle': 'Estilo de Vida',
			'sustainability': 'Sustentabilidade',
			'team': 'Equipe',
			'book': 'Vídeo e Book Digital',
			'parque-das-americas': 'Parque das Américas',
			'location': 'Localização',
			'timeline': 'Andamento da Obra',
			/* 'blog': 'Blog', */
			'contact': 'Contato'

		}
	},
	timeline_state: {
		active: false,
		current: 'timeline',
		item:{
			'timeline': 'Andamento da Obra',
		}
	},

	getters: {
		//home getters
		getActive(state) {
			return state.active
		},

		getCurrent(state) {
			return state.current
		},

		getItems(state) {
			return state.items
		},

		getAnchors(state) {
			let anchors = Object.keys(state.items)

			anchors.unshift('hero')
			return 	anchors
		},

		//timeline getters
		getTimelineActive(timeline_state) {
			return timeline_state.active
		},

		getTimelineCurrent(timeline_state) {
			return timeline_state.current
		},

		getTimelineItems(timeline_state) {
			return timeline_state.items
		},

		getTimelineAnchor(timeline_state) {
			let anchors = Object.keys(timeline_state.item)

			anchors.unshift('timeline')
			return 	anchors
		}
	},

	mutations: {
		setActive(state, value) {
			Vue.set(state, 'active', value)
		},

		setCurrent(state, value) {
			Vue.set(state, 'current', value)
		},

		previous(state) {
			let items = Object.keys(state.items),
				current = state.current,
				index = items.indexOf(current)

			if (index - 1 > 0)
				Vue.set(state, 'current', items[index - 0])
		},

		next(state) {
			let items = Object.keys(state.items),
				current = state.current,
				index = items.indexOf(current)

			if (index + 1 < items.length)
				Vue.set(state, 'current', items[index + 1])
		}
	}
}

