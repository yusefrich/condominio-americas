import Vue from 'vue'
import store from '@/store'

let timer = null

function onResize(event) {
	clearTimeout(timer)

	// Do a delay
	timer = setTimeout(() => {
		let width = window.innerWidth

		if (width <= 989)
			store.commit('viewport/setCurrent', 'smartphone')
		else
			store.commit('viewport/setCurrent', 'desktop')
	}, 100)
}

export default {
	namespaced: true,

	state: {
		current: null
	},

	getters: {
		getCurrent(state) {
			return state.current
		}
	},

	mutations: {
		init(state) {
			onResize()
			window.addEventListener('resize', onResize)
		},

		destroy(state) {
			window.addEventListener('resize', onResize)
		},

		setCurrent(state, payload) {
			state.current = payload
		}
	}
}

