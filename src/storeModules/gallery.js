import Vue from 'vue'
import store from '@/store'

export default {
	namespaced: true,

	state: {
		current: 0,
		active: false,
		images: []
	},

	getters: {
		getActive(state) {
			return state.active
		},

		getImages(state) {
			return state.images
		},

		getCurrent(state) {
			return state.current
		}
	},

	mutations: {
		setCurrent(state, value) {
			Vue.set(state, 'current', value)
		},

		setImages(state, value) {
			Vue.set(state, 'current', value)
		},

		open(state, payload) {
			if (payload.images) {
				Vue.set(state, 'images', payload.images)
				Vue.set(state, 'current', payload.current)
			} else {
				Vue.set(state, 'images', payload)
			}

			Vue.set(state, 'active', true)
		},

		close(state) {
			Vue.set(state, 'images', [])
			Vue.set(state, 'active', false)
		}
	}
}

